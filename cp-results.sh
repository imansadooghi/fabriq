#!/bin/bash

rm $HOME/results/push/*
rm $HOME/results/pop/*
while read line
do
    name=$line
    ssh -n $name scp $HOME/push.txt ubuntu@10.123.168.70:$HOME/results/push/$name
    ssh -n $name scp $HOME/pop.txt ubuntu@10.123.168.70:$HOME/results/pop/$name
	
done < ip.list

cat $HOME/results/push/* > $HOME/pushResults.txt
cat $HOME/results/pop/* > $HOME/popResults.txt

scp ~/pushResults.txt iman@216.47.142.109:/home/iman/fabriqResults/.
scp ~/popResults.txt iman@216.47.142.109:/home/iman/fabriqResults/.


