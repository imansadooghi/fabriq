+# README #
+
+The advent of Big Data has brought many challenges and opportunities in distributed systems, which have only amplified with the rate of  growth of data. There is a need to rethink the software stack for supporting data intensive computing and big data analytics. Over the past decade, the data analytics applications have turned to finer granular tasks which are shorter in duration and much more in quantity. Such
+applications require new frameworks to handle their data flow.
+Distributed Message Queues have proven to be essential building blocks in distributed computing towards the support for fine granular workloads. Distributed message queues such as Amazon’s SQS or Apache’s Kafka have been used in handling massive data volumes, content delivery, and many more. They have also been used in large scale job scheduling on public clouds. However, even these frameworks have some limitations that make them incapable of handling large scale data with high efficiency and low latency. We propose Fabriq, a distributed message queue that runs on top of a Distributed Hash Table. The design goal of Fabriq is to achieve scalability and near perfect load balancing, while handling the operations with minimal overhead and low latency. Moreover, Fabriq is persistent,
+reliable and consistent. The results show that Fabriq was able to achieve high throughput in both small and large messages. At
Add a comment to this line
+128 nodes scale, Fabriq’s throughput was more than 90,000 messages/sec for 50 bytes messages. At the same scale, Fabriq’s latency was less than 1 millisecond. Our framework outperforms other state of the art systems including Kafka and SQS in throughput and latency. Furthermore, our experiments show that Fabriq provides a significantly better load balancing than Kafka. 
+
+* Version 1.0
+
+### How do I get set up? ###
+
+* Dependencies
+In order to install Fabriq, you need to have Google Protocol Buffer and Google Protocol Buffer C bindings installed.
+
+* Configuration
+
+* Deployment instructions
+will be posted soon
+
+### Who do I talk to? ###
+
+Repository Admin: Iman Sadooghi